# h - help
h help:
	@echo "h help 	- this help"
	@echo "teste2e 	- run e2e integration tests"
.PHONY: h


teste2e:
	testbox -f tests/e2e_testbox/config.yaml -v
.PHONY: teste2e
