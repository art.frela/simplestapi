package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
)

const (
	envPAR1 = "SIMPLESTAPI_PAR1"
	envPAR2 = "SIMPLESTAPI_PAR2"
	defPAR1 = "default value for SIMPLESTAPI_PAR1"
	defPAR2 = "default value for SIMPLESTAPI_PAR2"
	defPORT = "8080"
)

func main() {
	port := flag.String("p", defPORT, "TCP port for http server")
	// Setup
	e := echo.New()
	e.HidePort = true
	e.HideBanner = true // hide banner ECHO
	e.Use(middleware.Recover())
	e.Use(middleware.RequestID())
	e.Logger.SetLevel(log.DEBUG)
	e.GET("/api/v1/params", getPAR)
	e.POST("/api/v1/params", postPAR)
	e.PUT("/api/v1/params", putPAR)
	e.DELETE("api/v1/params/:env/", deletePAR)

	// Start server
	go func() {
		e.Logger.Infof("http server started on :%s", *port)
		if err := e.Start(":" + *port); err != nil && err != http.ErrServerClosed {
			e.Logger.Fatal("shutting down the server")
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with a timeout of 10 seconds.
	// Use a buffered channel to avoid missing signals as recommended for signal.Notify
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	if err := e.Shutdown(ctx); err != nil {
		e.Logger.Fatal(err)
	}
}

func getPAR(c echo.Context) error {
	param := c.QueryParam("parameter")
	switch param {
	case envPAR1:
		v := os.Getenv(envPAR1)
		if v == "" {
			v = defPAR1
		}
		return c.String(http.StatusOK, v)
	case envPAR2:
		v := os.Getenv(envPAR2)
		if v == "" {
			v = defPAR2
		}
		return c.String(http.StatusOK, v)
	default:
		v := os.Getenv(param)
		if v == "" {
			v = fmt.Sprintf("does not set value for env [%s]", param)
		}
		return c.String(http.StatusOK, v)
	}
}

type ENV struct {
	K string `json:"k"`
	V string `json:"v"`
}

type ENVS []ENV

func postPAR(c echo.Context) error {
	params := ENVS{}
	if err := c.Bind(&params); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	response := make([]string, 0, len(params))
	for _, e := range params {
		k := strings.ToUpper(e.K)
		v := e.V
		response = append(response, k+"="+v)
		err := os.Setenv(k, v)
		if err != nil {
			return c.String(http.StatusInternalServerError, err.Error())
		}
	}
	return c.JSON(http.StatusCreated, response)
}

func putPAR(c echo.Context) error {
	e := ENV{}
	if err := c.Bind(&e); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	response := make([]string, 0)

	k := strings.ToUpper(e.K)
	v := e.V
	response = append(response, k+"="+v)
	err := os.Setenv(k, v)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, response)
}

func deletePAR(c echo.Context) error {
	param := c.Param("env")
	if param == "" {
		return c.String(http.StatusBadRequest, "empty parameter env")
	}
	os.Unsetenv(param)
	return c.NoContent(http.StatusNoContent)
}
